import { Dependencies, Injectable } from '@nestjs/common';

import { DaprPubSubService } from './dapr-pub-sub.service';
import { DaprInvokeService } from './dapr-invoke.service';

@Injectable()
@Dependencies(DaprPubSubService, DaprInvokeService)
export class DaprService {
    constructor(pubsub, invoke) {
        this.pubsub = pubsub;
        this.invoke = invoke;
    }
}
