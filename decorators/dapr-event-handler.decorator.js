import { applyDecorators, SetMetadata } from '@nestjs/common';

export const DAPR_EVENT_HANDLER = 'DaprEventHandler';

export const DaprEventHandler = (topic, options = {}) => {
    const topics = (Array.isArray(topic) ? topic : [topic]).filter((v) => v);

    return applyDecorators(
        SetMetadata(DAPR_EVENT_HANDLER, {
            topics,
            options,
        })
    );
};
