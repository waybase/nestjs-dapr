import { applyDecorators, SetMetadata, Bind, Body } from '@nestjs/common';

export const DAPR_SUBSCRIBER = 'DaprSubscriber';

export const DaprSubscriber = (topic, options = {}) => {
    const topics = (Array.isArray(topic) ? topic : [topic]).filter((v) => v);

    return applyDecorators(
        SetMetadata(DAPR_SUBSCRIBER, {
            topics,
            options,
        }),
        Bind(Body())
    );
};
