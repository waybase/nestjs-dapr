import { applyDecorators, SetMetadata, Bind, Body } from '@nestjs/common';

export const DAPR_ALL_SUBSCRIBER = 'DaprAllSubscriber';

export const DaprAllSubscriber = (options = {}) =>
    applyDecorators(
        SetMetadata(DAPR_ALL_SUBSCRIBER, { options }),
        Bind(Body())
    );
