export const DEFAULT_HOST = 'localhost';
export const DEFAULT_HTTP_PORT = 3500;
export const BINDINGS_ENDPOINT = '/v1.0/bindings';
export const INVOKE_ENDPOINT = '/v1.0/invoke';
export const PUBLISH_ENDPOINT = '/v1.0/publish';
export const SECRETS_ENDPOINT = '/v1.0/secrets';
export const STATE_ENDPOINT = '/v1.0/state';

export const DEFAULT_PUBSUBNAME = 'pubsub';
