import { Injectable } from '@nestjs/common';

import { DEFAULT_PUBSUBNAME } from './constants';

@Injectable()
export class DaprConfigService {
    constructor(options) {
        this.pubsub = {
            pubsubname: DEFAULT_PUBSUBNAME,
            topicPrefix: '',
            ...options?.pubsub,
        };
    }
}
