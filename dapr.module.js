import {
    Dependencies,
    Global,
    HttpModule,
    Logger,
    Module,
    RequestMethod,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import {
    DiscoveryModule,
    DiscoveryService,
    MetadataScanner,
} from '@nestjs/core';

import { METHOD_METADATA, PATH_METADATA } from '@nestjs/common/constants';
import { validatePath } from '@nestjs/common/utils/shared.utils';
import {
    DAPR_SUBSCRIBER,
    DaprSubscriber,
    DAPR_EVENT_HANDLER,
    DaprEventHandler,
    DAPR_ALL_SUBSCRIBER,
    DaprAllSubscriber,
} from './decorators';

import { DaprService } from './dapr.service';

import { DEFAULT_HOST, DEFAULT_HTTP_PORT } from './constants';
import { DaprController } from './dapr.controller';

import { DaprConfigService } from './dapr-config.service';
import { DaprPubSubService } from './dapr-pub-sub.service';
import { DaprInvokeService } from './dapr-invoke.service';

const validateGlobalPath = (path) => {
    const prefix = validatePath(path);
    return prefix === '/' ? '' : prefix;
};

const validateRoutePath = (path) => {
    return validatePath(path);
};

@Global()
@Module({
    imports: [
        DiscoveryModule,
        HttpModule.registerAsync({
            useFactory: async (configService) => ({
                baseURL: `http://${configService.get(
                    'DAPR_HOST',
                    DEFAULT_HOST
                )}:${+configService.get('DAPR_HTTP_PORT', DEFAULT_HTTP_PORT)}/`,
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [DaprController],
    providers: [
        DaprService,
        DaprPubSubService,
        DaprInvokeService,
        DaprConfigService,
    ],
    exports: [DaprService, DaprPubSubService, DaprInvokeService],
})
@Dependencies(DiscoveryService, MetadataScanner, DaprService)
export class DaprModule {
    constructor(discovery, metadataScanner, service) {
        this.discovery = discovery;
        this.metadataScanner = metadataScanner;
        this.service = service;
        this.logger = new Logger(DaprModule.name);
    }

    static createAsyncOptionsProvider(options) {
        if (options.useFactory) {
            return [
                {
                    provide: DaprConfigService,
                    useFactory: async (...factoryArgs) =>
                        new DaprConfigService(
                            await options.useFactory(...factoryArgs)
                        ),
                    inject: [...(options.inject ?? [])],
                },
            ];
        }

        return [
            {
                provide: DaprConfigService,
                useFactory: async (optionsFactory) =>
                    new DaprConfigService(
                        await optionsFactory.createDaprOptions()
                    ),
                inject: [options.useExisting],
            },
        ];
    }

    static forRootAsync(options) {
        const providers = this.createAsyncOptionsProvider(options);

        return {
            module: DaprModule,
            imports: [
                HttpModule.registerAsync({
                    useFactory: async (configService) => ({
                        baseURL: `http://${configService.get(
                            'DAPR_HOST',
                            DEFAULT_HOST
                        )}:${+configService.get(
                            'DAPR_HTTP_PORT',
                            DEFAULT_HTTP_PORT
                        )}/`,
                    }),
                    inject: [ConfigService],
                }),
            ],
            providers,
        };
    }

    onModuleInit() {
        const handlers = this.discovery
            .getProviders()
            .map(({ instance }) => {
                if (!instance) {
                    return [];
                }

                const prototype = Object.getPrototypeOf(instance);

                return this.metadataScanner.scanFromPrototype(
                    instance,
                    prototype,
                    (methodName) => {
                        const method = prototype[methodName];

                        const handler = Reflect.getMetadata(
                            DAPR_EVENT_HANDLER,
                            method
                        );

                        if (!handler) {
                            return undefined;
                        }

                        const { topics, options } = handler;

                        return {
                            handler: method.bind(instance),
                            topics,
                            options,
                        };
                    }
                );
            })
            .flat();

        handlers.forEach(({ topics, handler, options }) =>
            topics.forEach((topic) =>
                this.service.pubsub.registerHandler(topic, handler, options)
            )
        );

        const allTopics = handlers
            .map(({ topics }) => topics)
            .flat()
            .filter((item, index, self) => self.indexOf(item) === index);

        const subscriptions = this.discovery
            .getControllers()
            .map(({ instance, metatype }) => {
                if (!instance) {
                    return [];
                }
                const prototype = Object.getPrototypeOf(instance);

                const globalPath = validateGlobalPath(
                    Reflect.getMetadata(PATH_METADATA, metatype)
                );

                return this.metadataScanner
                    .scanFromPrototype(instance, prototype, (methodName) => {
                        const method = prototype[methodName];
                        const routePath = Reflect.getMetadata(
                            PATH_METADATA,
                            method
                        );

                        if (!routePath) {
                            return undefined;
                        }

                        const requestMethod = Reflect.getMetadata(
                            METHOD_METADATA,
                            method
                        );

                        if (requestMethod !== RequestMethod.POST) {
                            return undefined;
                        }

                        const subscriber = Reflect.getMetadata(
                            DAPR_SUBSCRIBER,
                            method
                        );

                        if (!subscriber?.topics?.length) {
                            return undefined;
                        }

                        const fullPath = `${globalPath}${validateRoutePath(
                            routePath === '/' ? '' : routePath
                        )}`;

                        const { pubsubname } = subscriber.options;

                        return subscriber.topics.map((topic) => ({
                            pubsubname,
                            route: fullPath === '' ? '/' : fullPath,
                            topic,
                        }));
                    })
                    .filter((m) => m)
                    .flat();
            })
            .flat();

        subscriptions.forEach((subscription) =>
            this.service.pubsub.subscribe(subscription)
        );

        const allSubscriptions = this.discovery
            .getControllers()
            .map(({ instance, metatype }) => {
                if (!instance) {
                    return [];
                }
                const prototype = Object.getPrototypeOf(instance);

                const globalPath = validateGlobalPath(
                    Reflect.getMetadata(PATH_METADATA, metatype)
                );

                return this.metadataScanner
                    .scanFromPrototype(instance, prototype, (methodName) => {
                        const method = prototype[methodName];
                        const routePath = Reflect.getMetadata(
                            PATH_METADATA,
                            method
                        );

                        if (!routePath) {
                            return undefined;
                        }

                        const requestMethod = Reflect.getMetadata(
                            METHOD_METADATA,
                            method
                        );

                        if (requestMethod !== RequestMethod.POST) {
                            return undefined;
                        }

                        const subscriber = Reflect.getMetadata(
                            DAPR_ALL_SUBSCRIBER,
                            method
                        );

                        if (!subscriber) {
                            return undefined;
                        }

                        const fullPath = `${globalPath}${validateRoutePath(
                            routePath === '/' ? '' : routePath
                        )}`;

                        const { pubsubname } = subscriber.options;

                        return allTopics.map((topic) => ({
                            pubsubname,
                            route: fullPath === '' ? '/' : fullPath,
                            topic,
                        }));
                    })
                    .filter((m) => m)
                    .flat();
            })
            .flat();

        allSubscriptions.forEach((subscription) =>
            this.service.pubsub.subscribe(subscription)
        );
    }
}

export {
    DaprService,
    DaprPubSubService,
    DaprInvokeService,
    DaprSubscriber,
    DaprAllSubscriber,
    DaprEventHandler,
};
