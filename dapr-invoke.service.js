import { Dependencies, HttpService, Injectable } from '@nestjs/common';
import url from 'url';

import { INVOKE_ENDPOINT } from './constants';

@Injectable()
@Dependencies(HttpService)
export class DaprInvokeService {
    constructor(httpService) {
        this.httpService = httpService;
    }

    async invokeMethod(
        appId,
        methodName,
        { data, method = 'GET', headers } = {}
    ) {
        const methodUrl =
            typeof methodName === 'object'
                ? url.format(methodName)
                : methodName;

        try {
            const response = await this.httpService.instance({
                method,
                data,
                headers,
                url: `${INVOKE_ENDPOINT}/${appId}/method/${methodUrl}`,
            });

            return response.data;
        } catch (error) {
            console.log(error);

            throw new Error(
                `Unable to invokeMethod, ${method} ${appId} ${methodUrl}`
            );
        }
    }
}
