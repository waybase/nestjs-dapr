import { Controller, Dependencies, Get } from '@nestjs/common';

import { DaprService } from './dapr.service';

@Controller('dapr')
@Dependencies(DaprService)
export class DaprController {
    constructor(service) {
        this.service = service;
    }

    @Get('subscribe')
    async getSubscriptions() {
        return this.service.pubsub.subscriptions;
    }
}
