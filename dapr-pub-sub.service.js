import { Dependencies, HttpService, Injectable } from '@nestjs/common';

import { PUBLISH_ENDPOINT } from './constants';
import { DaprConfigService } from './dapr-config.service';

@Injectable()
@Dependencies(HttpService, DaprConfigService)
export class DaprPubSubService {
    constructor(httpService, config) {
        this.options = config.pubsub;

        this.httpService = httpService;
    }

    subscriptions = [];

    handlers = {};

    async publishEvent(
        topic,
        data,
        { pubsubname = this.options.pubsubname } = {}
    ) {
        try {
            await this.httpService.instance({
                method: 'POST',
                data,
                url: `${PUBLISH_ENDPOINT}/${pubsubname}/${topic}`,
            });
        } catch (error) {
            throw new Error('Unable to publishEvent');
        }
    }

    subscribe({ pubsubname = this.options.pubsubname, topic, route }) {
        this.subscriptions.push({
            pubsubname,
            topic,
            route,
        });
    }

    registerHandler(topic, handler) {
        this.handlers = {
            ...this.handlers,
            [topic]: [...(this.handlers[topic] ?? []), handler],
        };
    }

    async handleEvent(event) {
        const { topic } = event;

        const handlers = this.handlers[topic] ?? [];

        const { data, ...metadata } = event;

        return Promise.all(handlers.map((handler) => handler(data, metadata)));
    }
}
